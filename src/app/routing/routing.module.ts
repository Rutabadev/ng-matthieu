import { HomeComponent } from './../home/home.component';
import { CssShowcaseComponent } from './../css-showcase/css-showcase.component';
import { NgModule } from '@angular/core';
import { NotFoundComponent } from './../not-found/not-found.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'css',
    component: CssShowcaseComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
