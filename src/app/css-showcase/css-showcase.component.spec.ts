import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CssShowcaseComponent } from './css-showcase.component';

describe('CssShowcaseComponent', () => {
  let component: CssShowcaseComponent;
  let fixture: ComponentFixture<CssShowcaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CssShowcaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CssShowcaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
